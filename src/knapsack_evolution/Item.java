package knapsack_evolution;

public class Item {
    
    private int weight;
    private int cost;
    private double ratio;
    private boolean presence;
    
    public Item(int weight, int cost, boolean presence){
       this.weight = weight;
       this.cost = cost;
       this.ratio = ((double)cost / (double)weight);       
       this.presence = presence;
    }
    
    public Item clone(){
        Item i = new Item(this.weight, this.cost, this.presence);
        return i;
    }
    
    public void setPresence(boolean flag){
        this.presence = flag;
    }
    
    public boolean getPresence(){
        return presence;
    }
    
    public int getWeight(){
        return weight;
    }
    
    public int getCost(){
        return cost;
    }
    
    public double getRatio(){
        return ratio;
    }
    
    
    
}
